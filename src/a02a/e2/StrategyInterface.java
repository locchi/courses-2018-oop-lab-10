package a02a.e2;

public interface StrategyInterface {
	
	int compute(int i);
}
