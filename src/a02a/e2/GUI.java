package a02a.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
	
	private List<Boolean> list;
	private List<JButton> jbuttonlist = new ArrayList<>();
	private int i;
    
    public GUI(int size) {
    	this.list = new ArrayList<>();
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        int cols = size; // {1,2,3,4,5}
        JPanel panel = new JPanel(new GridLayout(cols,cols));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            for(i=0;i<size*size;i++) {
        		if(bt.equals(jbuttonlist.get(i))) {
        			break;
        		}
        	}
            if(bt.getText().equals("*")) {
            	bt.setText("");
    			list.add(i, false);
    			list.remove(i+1);
            }
            else {
            	bt.setText("*");
    			list.add(i, true);
    			list.remove(i+1);
            }            
            
            int riga = (i/size) * size;
            int colonna = i%size;
            int contriga = 0;
            int contcolon = 0;
            
            for(int i = 0; i<size; i++) {
            	if(list.get(riga+i) == true) {
            		contriga++;
            	}
            	if(list.get(colonna+size*i) == true) {
            		contcolon++;
            	}
            }
            if(contriga==size || contcolon == size) {
            	System.exit(1);
            }
        };
        for (int i=0;i<cols*cols;i++){
            final JButton jb = new JButton("");
            jb.addActionListener(al);
            panel.add(jb);
            this.list.add(i, false);
            this.jbuttonlist.add(i, jb);
        } 
        this.setVisible(true);
    }
    
}
