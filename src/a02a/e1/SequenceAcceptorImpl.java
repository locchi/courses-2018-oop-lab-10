package a02a.e1;

import java.util.ArrayList;
import java.util.List;

public class SequenceAcceptorImpl implements SequenceAcceptor {

	private String name;
	private List<Integer> list;
	private int cont;
	
	
	@Override
	public void reset(Sequence sequence) {
		this.name = sequence.name();
		this.cont = 1;
		if(sequence.name().equals(Sequence.RAMBLE.name())){
			this.list = new ArrayList<>();
			list.add(0);
			list.add(1);
			cont = 0;
		}
		if(sequence.name().equals(Sequence.FIBONACCI.name())){
			this.list = new ArrayList<>();
			list.add(1);
			list.add(1);
			this.cont = 0;
		}
	}

	@Override
	public void reset() {
		cont = 1;
		if(this.name.equals(Sequence.RAMBLE.name())){
			this.list = new ArrayList<>();
			list.add(0);
			list.add(1);
			cont = 0;
		}
		if(this.name.equals(Sequence.FIBONACCI.name())){
			this.list = new ArrayList<>();
			list.add(1);
			list.add(1);
			this.cont = 0;
		}
	}

	@Override
	public void acceptElement(int i) {
		if(this.name.equals(Sequence.POWER2.name())) {
			if(this.cont == i) {
				cont *=2;
			}
			else {
				throw new IllegalStateException();
			}
		}
		else if(this.name.equals(Sequence.FLIP.name())) {
			if(this.cont == i) {
				cont = (cont+1)%2;
			}
			else {
				throw new IllegalStateException();
			}
		}
		else if(this.name.equals(Sequence.RAMBLE.name())) {
			if(list.get(cont) != i) {
				throw new IllegalStateException();
			}
			if(cont != 0) {
				if(list.get(cont) == 0) {
					list.add(cont +1, list.get(cont -1) + 1);
				}
				else {
					list.add(cont+1, 0);
				}
				
			}
			cont++;
		}
		else if(this.name.equals(Sequence.FIBONACCI.name())) {
			if(list.get(cont)!= i) {
				throw new IllegalStateException();
			}
			else if(cont != 0) {
				list.add(cont+1, list.get(cont)+list.get(cont-1));
				
			}
			cont++;
		}
		else {
			throw new IllegalStateException();
		}

	}

}
